## Install sshpass

```
brew install http://git.io/sshpass.rb
```

## Install K3S to Raspberry Pi 4

```
ansible-playbook k3s_operator.yml --ask-pass
```